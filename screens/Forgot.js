import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import * as firebase from 'firebase'
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";

const { width, height } = Dimensions.get("screen");

// Your web app's Firebase configuration
  const firebaseConfig = {
  apiKey: "AIzaSyDfgdXfU-PmG3xhzy60tN0Ybtxnr_95xBk",
  authDomain: "pim-react-native.firebaseapp.com",
  databaseURL: "https://pim-react-native.firebaseio.com",
  projectId: "pim-react-native",
  storageBucket: "pim-react-native.appspot.com",
  messagingSenderId: "400133452288",
  appId: "1:400133452288:web:a19c707687b66b67efdee5",
  measurementId: "G-1Q18HM55SZ"
};
  // Initialize Firebase
if ( ! firebase . apps . length ) {
   firebase.initializeApp(firebaseConfig);
    // firebase.analytics();

}



class Register extends React.Component {
constructor (props){
    super(props)
    this.state=({
        email:'',
        password:''
            })
}
componentWillUpdate() {

    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        console.log(user)
      }
    })
  }
forgotPassword = (email) => {
    firebase.auth().sendPasswordResetEmail(email)
      .then(function (user) {
        alert('Please check your email...')
      }).catch(function (e) {
        console.log(e)
      })
  }


  render() {
    return (
      <Block flex middle>
        <StatusBar hidden />
        <ImageBackground
          source={Images.RegisterBackground}
          style={{ width, height, zIndex: 1 }}
        >
          <Block flex middle>
            <Block style={styles.registerContainer}>
              
              
              <Block flex>
                <Block flex={0.17} middle>
                  <Text color="#8898AA" style = {{fontSize:20,fontWeight:'bold', marginTop: 40}}>
                    Reset password
                  </Text>
                  <Text color="#8898AA" style = {{fontSize:16,fontWeight:'bold', marginTop: 30}}>
                      Please enter your email address
                      to request a password reset 
                  </Text>
                </Block>




                
                <Block center style={{ marginTop: 80 }}>
                  <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior="padding"
                    enabled
                  >
                   
                   
                    <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                      <Input
                        borderless
                        
                        placeholder="Email"
                         onChangeText={(email)=>this.setState({email})}
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="ic_mail_24px"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                    </Block>
                   
                   
                    
                    
                    <Block middle>
                      <Button 
                                onPress={()=>this.forgotPassword(this.state.email)}
                      color="primary" style={styles.createButton}>
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          SEND
                        </Text>
                      </Button>
                    </Block>
                  </KeyboardAvoidingView>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default Register;
