// local imgs
const Onboarding = require("../assets/imgs/ko.jpg");
const Logo = require("../assets/imgs/argon-logo.png");
const LogoOnboarding = require("../assets/imgs/Billeterie-Icon-White.png");
const ProfileBackground = require("../assets/imgs/bg-profile.jpg");
const RegisterBackground = require("../assets/imgs/Background.jpg");
const Pro = require("../assets/imgs/getPro-bg.png");
const ArgonLogo = require("../assets/imgs/bb.png");
const iOSLogo = require("../assets/imgs/ios.png");
const androidLogo = require("../assets/imgs/android.png");
// internet imgs

const ProfilePicture = 'https://scontent.ftun9-1.fna.fbcdn.net/v/t1.0-9/s960x960/55453602_415490322361341_6466409461283028992_o.jpg?_nc_cat=104&_nc_ohc=UqPziPDpsp8AX-B4sg9&_nc_ht=scontent.ftun9-1.fna&_nc_tp=7&oh=9508eab757161a43c62ddfeaed4d2c5f&oe=5EFB9DB8';

const Viewed = [
  'https://images.unsplash.com/photo-1501601983405-7c7cabaa1581?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1543747579-795b9c2c3ada?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1551798507-629020c81463?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1470225620780-dba8ba36b745?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1503642551022-c011aafb3c88?fit=crop&w=240&q=80',
  'https://images.unsplash.com/photo-1482686115713-0fbcaced6e28?fit=crop&w=240&q=80',
];

const Products = {
  'View article': 'https://images.unsplash.com/photo-1501601983405-7c7cabaa1581?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=840&q=840',
};

export default {
  Onboarding,
  Logo,
  LogoOnboarding,
  ProfileBackground,
  ProfilePicture,
  RegisterBackground,
  Viewed,
  Products,
  Pro,
  ArgonLogo,
  iOSLogo,
  androidLogo
};