export default [
  {
    title: 'League Pro : EST vs CSS',
    image: 'https://sport.tunisienumerique.com/wp-content/uploads/2019/03/est-css.jpg',
    cta: 'View details', 
    horizontal: true
  },
  {
    title: 'League Pro : CA vs ESS',
    image: 'https://content.mosaiquefm.net/uploads/content/thumbnails/ca_vs_ess_ne_se_jouera_pas_a_tunis_1547894095.jpg',
    cta: 'View details'
  },
  {
    title: 'League Pro : JSK vs ESS',
    image: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_3.0,f_auto,fl_lossy,q_auto:good,w_500/v1/polls/d1y7hq7paonne27r4tmh',
    cta: 'View details' 
  },
  {
    title: 'League Pro : CA vs ESS',
    image: 'https://www.moujafm.com/wp-content/uploads/2019/02/maxresdefault-14.jpg',
    cta: 'View details' 
  },
  {
    title: 'League Pro : CSS vs ESS',
    image: 'https://www.realites.com.tn/wp-content/uploads/2019/06/CSS-EST-1280x720.jpg',
    cta: 'View details', 
    horizontal: true
  },
];